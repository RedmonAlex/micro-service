const UserController = require('../controller/UserController')
const express = require("express")
let router = express.Router()

router.get('/people',UserController.fetchPeople)
module.exports = router