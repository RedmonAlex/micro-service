const faker = require('faker');

const generatePerson = ()=>({
    name: faker.name.findName(),
    email:faker.internet.email(),
    address: faker.address.streetAddress()
})

exports.gereatePerson = generatePerson
exports.generatePeople = ()=> new Array(100).fill(1,0,100).map(x=>generatePerson())